package esercizio;

public class ContattoImpl implements Comparable<ContattoImpl>, Contatto {

	private final String nome;
	private final String cognome;
	private final String telefono;
	
	public ContattoImpl(String nome, String cognome, String telefono) {
		this.nome = nome;
		this.cognome = cognome;
		this.telefono = telefono;
	}
	
	@Override
	public String getNome() {
		return nome;
	}

	@Override
	public String getCognome() {
		return cognome;
	}

	@Override
	public String getTelefono() {
		return telefono;
	}
	
	@Override
	public String toString() {
		return "[nome=" + this.nome + ",cognome=" + 
				this.cognome + ",telefono=" + this.telefono + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cognome == null) ? 0 : cognome.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContattoImpl other = (ContattoImpl) obj;
		if (cognome == null) {
			if (other.cognome != null)
				return false;
		} else if (!cognome.equals(other.cognome))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public int compareTo(ContattoImpl o) {
		return (cognome + nome).compareTo(o.getCognome() + o.getNome());
	}

	
	
}
