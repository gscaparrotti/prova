package esercizio;

import java.util.HashSet;
import java.util.Set;

//modifica
public class RubricaImpl implements Rubrica {
	
	private final HashSet<ContattoImpl> contatti;
	
	public RubricaImpl() {
		this.contatti = new HashSet<>();
	}
	
	@Override
	public void aggiungiContatto(String nome, String cognome, String telefono) {
		contatti.add(new ContattoImpl(nome, cognome, telefono));
	}

	@Override
	public void rimuoviContatto(String nome, String cognome) {
		Contatto temp = new ContattoImpl(nome, cognome, null);
		contatti.remove(temp);
	}
	
	@Override
	public String cerca(String nome, String cognome) {
		for(Contatto c : contatti) {
			if(c.getNome().equals(nome) &&
					c.getCognome().equals(cognome)) {
				return c.getTelefono();
			}
		}
		return null;
	}
	
	@Override
	public Set<ContattoImpl> getAll() {
		return new HashSet<ContattoImpl>(contatti);
	}
	
	@Override
	public String toString() {
		String string = "{";
		for (Contatto c : contatti) {
			string = string + c.toString();
		}
		return string + "}";
	}
}
