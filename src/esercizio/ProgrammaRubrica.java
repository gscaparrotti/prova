package esercizio;

public class ProgrammaRubrica {

	public static void main(String[] args) {

		final Rubrica agendaElettronica = new RubricaImpl();
		agendaElettronica.aggiungiContatto("Giacomo","Scaparrotti","0541-123456");
		agendaElettronica.aggiungiContatto("Mario", "Rossi", "0541-456789");
		System.out.println(agendaElettronica.cerca("Giacomo", "Scaparrotti"));
		System.out.println(agendaElettronica.cerca("Mario", "Rossi"));
		System.out.println(agendaElettronica);
		agendaElettronica.rimuoviContatto("Giacomo", "Scaparrotti");
		System.out.println(agendaElettronica.cerca("Giacomo", "Scaparrotti"));
		System.out.println(agendaElettronica.cerca("Mario", "Rossi"));
		System.out.println(agendaElettronica);
	}
}