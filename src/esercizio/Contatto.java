package esercizio;

public interface Contatto {

	String getNome();

	String getCognome();

	String getTelefono();

}