package esercizio;

public class ProvaComparable {

	public static void main(String[] args) {
		ContattoImpl mariorossi = new ContattoImpl("mario", "rossi", null);
		ContattoImpl giuseppeverdi = new ContattoImpl("giuseppe", "verdi", null);
		ContattoImpl lucabianchi = new ContattoImpl("luca", "bianchi", null);
		System.out.println(mariorossi.compareTo(giuseppeverdi));
		System.out.println(mariorossi.compareTo(lucabianchi));
		System.out.println(mariorossi.compareTo(mariorossi));
	}

}
