package esercizio;

import java.util.Set;

public interface Rubrica {

	void aggiungiContatto(String nome, String cognome, String telefono);

	void rimuoviContatto(String nome, String cognome);

	String cerca(String nome, String cognome);

	Set<ContattoImpl> getAll();

}